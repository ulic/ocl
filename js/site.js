/*jshint browser: true, jquery: true*/
/* global WURFL: false, Drupal: false */

(function ($) {

  $(function () {

    var isotopeMain = null,
        resized = null,
        originalWindowHeight = $(window).height();

    function mainLayout() {
      if ($('body.page-node-103').length) {
        $('.main-content').css('visibility', 'visible');
        return;
      }

      if ($(window).width() >= 960 || $('nav.menu-mission-proposition').length || $('nav.menu-40-days-of-prayer').length) {
        var filter = ($('nav.menu-mission-proposition').length) ? '.filter-past' : '*';
        if (!isotopeMain) {
          isotopeMain = $('.main-content .content').isotope({
            filter: filter,
            itemSelector: '.content-item',
            percentPosition: true,
            stamp: '.stamp',
            masonry: {
              columnWidth: '.content-item-sizer',
              gutter: '.content-item-gutter'
            }
          });
          if($('.menu-40-days-of-prayer').length) { setup40days(); }
        }
        $('.main-content').imagesLoaded(function () {
          isotopeMain.isotope('layout');
        });
        isotopeMain.on('arrangeComplete', function() {
          $('.main-content').css('visibility', 'visible');
        });
        isotopeMain.on('layoutComplete', function() {
          $('.main-content').css('visibility', 'visible');
        });
      } else if (isotopeMain) {
        isotopeMain.isotope('destroy');
      }
    }

    function screenResize() {
      var currentWindowHeight = $(window).height();
      if (resized) {
        clearTimeout(resized);
      }
      resized = setTimeout(function () {
        if (WURFL.form_factor !== "Desktop" && currentWindowHeight === originalWindowHeight) {
          $('.background').height($(window).height() + 56);
        }
        mainLayout();
      }, 100);
    }

    function isIE() {
      return navigator.userAgent.match('Trident|MSIE');
    }

    // Events
    $(window).on('orientationchange', function () {
      setTimeout(function () {
        originalWindowHeight = $(window).height();
        screenResize();
      }, 150);
    });
    $(window).resize(screenResize);

    $('.nav-button').on('click', function () {
      $('.nav ul').slideToggle(300);
    });

    $('.page-wrapper').fitVids();

    $('.menu-mission-proposition li, .menu-40-days-of-prayer li').on('click', function () {
      var filter = $(this).attr('class');
      isotopeMain.isotope({
        filter: '.' + filter
      });
      $('li.'+filter).siblings().attr('data-state', 'inactive');
      $('li.'+filter).attr('data-state', 'active');
      screenResize();
    });

    function setup40days() {
      var $group = $('li[class|="filter"]');
      var filterFound = '*';
      $group.each(function (index, element) {
        var filter = $(element).attr('class');
        if (!$('div.'+filter).length) {
          $(element).hide();
        } else {
          filterFound = filter;
        }
      });
      $('li.'+filterFound).attr('data-state', 'active');
      isotopeMain.isotope({
        filter: '.' + filterFound
      });
    }

    // Run layout functions once initially
    $('.footer-item:not(.footer-item-full)').matchHeight();
    if (WURFL.form_factor === "Desktop" && !isIE()) {
      $('body').addClass('desktop');
    } else {
      $('body').addClass('tablet');
    }
    if (isIE()) {
      $('body').addClass('IE');
    }
    screenResize();

  });

  var views_infinite_scroll_was_initialised = false;
  Drupal.behaviors.views_infinite_scroll = {
    attach: function () {
      // Make sure that autopager plugin is loaded
      if ($('body').hasClass('not-front')) {
        return;
      }
      if ($.autopager) {
        if (!views_infinite_scroll_was_initialised) {
          views_infinite_scroll_was_initialised = true;
          var content_selector = '.main-content .content';
          var items_selector = content_selector + ' .content-item';
          $('ul.pager').hide();
          var handle = $.autopager({
            appendTo: content_selector,
            content: items_selector,
            link: '.pager-next a',
            page: 0,
            load: function () {
              $('div#views_infinite_scroll-ajax-loader').remove();
              Drupal.attachBehaviors(this);
              $('.main-content').fitVids();
              setTimeout(function () {
                $('.main-content .content').isotope();
              }, 150);
            }
          });

          // Trigger autoload if content height is less than doc height already
          var prev_content_height = $(content_selector).height();
          do {
            var last = $(items_selector).filter(':last');
            if (last.offset().top + last.height() < $(document).scrollTop() + $(window).height()) {
              last = $(items_selector).filter(':last');
              handle.autopager('load');
            } else {
              break;
            }
          }
          while ($(content_selector).height() > prev_content_height);
        }
      } else {
        //alert(Drupal.t('Autopager jquery plugin in not loaded.'));
      }
    }
  };

})(jQuery);
