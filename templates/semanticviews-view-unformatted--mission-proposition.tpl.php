<?php
/**
 * @file
 * Default simple view template to display a list of rows.
 *
 * @ingroup views_templates
 */
?>

<div class="content-item-sizer"></div>
<div class="content-item-gutter"></div>

<nav class="menu-mission-proposition stamp">
  <ul>
    <li class="filter-past" data-state="active">Past</li>
    <li class="filter-present" data-state="inactive">Present</li>
    <li class="filter-future" data-state="inactive">Future</li>
    <li class="filter-goals" data-state="inactive">Goals</li>
    <li class="filter-wins" data-state="inactive">Wins &amp; Outcomes</li>
  </ul>
</nav>

<?php foreach ($rows as $id => $row): ?>

  <div class="content-item <?php print strtolower($row_attributes[$id]['class']); ?>">
    <?php print $row; ?>
  </div>

<?php endforeach; ?>
