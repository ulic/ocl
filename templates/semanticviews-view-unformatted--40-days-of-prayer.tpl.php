<?php
/**
 * @file
 * Default simple view template to display a list of rows.
 *
 * @ingroup views_templates
 */
?>

<div class="content-item-sizer"></div>
<div class="content-item-gutter"></div>

<nav class="menu-40-days-of-prayer stamp">
  <ul>
    <li class="filter-week1" data-state="inactive">Week One</li>
    <li class="filter-week2" data-state="inactive">Week Two</li>
    <li class="filter-week3" data-state="inactive">Week Three</li>
    <li class="filter-week4" data-state="inactive">Week Four</li>
    <li class="filter-week5" data-state="inactive">Week Five</li>
    <li class="filter-week6" data-state="inactive">Week Six</li>
  </ul>
</nav>

<?php foreach ($rows as $id => $row): ?>

<div class="content-item <?php print strtolower($row_attributes[$id]['class']); ?>">
  <?php print $row; ?>
</div>

<?php endforeach; ?>
