<?php
/**
 * @file
 * Default simple view template to display a list of rows.
 *
 * @ingroup views_templates
 */
?>

<div class="content-item-sizer"></div>
<div class="content-item-gutter"></div>

<?php foreach ($rows as $id => $row): ?>

  <div class="content-item <?php print strtolower($row_attributes[$id]['class']); ?>">
    <?php print $row; ?>
  </div>

<?php endforeach; ?>
